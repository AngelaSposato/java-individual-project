module com.mycompany.realestatemanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens com.mycompany.realestatemanager to javafx.fxml;
    exports com.mycompany.realestatemanager;
}
