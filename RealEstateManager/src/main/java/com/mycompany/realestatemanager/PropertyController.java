/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

/**
 *
 * @author Angela Sposato 1934695
 */
public class PropertyController {
    
    @FXML
    private void switchToAddProperty() throws IOException {
        App.setRoot("add_property");
    }
    
    @FXML
    private void switchToMainMenu() throws IOException {
        App.setRoot("main_menu_final");
    }
    
    @FXML
    private void switchToDisplayProperty() throws IOException {
        App.setRoot("display_properties");
    }
    
    @FXML
    private void switchToViewProperty() throws IOException {
        App.setRoot("view_property");
    }
    
    @FXML 
    private void switchToMaintenanceHistory() throws IOException {
        App.setRoot("maintenance_history");
    }
}
