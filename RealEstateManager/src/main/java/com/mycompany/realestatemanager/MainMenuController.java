package com.mycompany.realestatemanager;

import java.io.IOException;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Angela Sposato 1934695
 */
public class MainMenuController {
    
    private HostServices hostServices ;

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices ;
    }
    @FXML
    public void switchScenes(ActionEvent event, String fileName) throws IOException{
        Parent listViewParent = FXMLLoader.load(getClass().getResource(fileName));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    private void switchToPropertyMenu(ActionEvent event) throws IOException {
       switchScenes(event, "display_properties.fxml");
    }
    
    @FXML
    private void switchToTenantMenu(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("display_tenants.fxml"));
        Parent tenantRoot = loader.load();
        DisplayTenantController controller = loader.getController();
        controller.setHostServices(hostServices);
        Stage tenant = (Stage)((Node)event.getSource()).getScene().getWindow();
        tenant.setScene(new Scene(tenantRoot));
        tenant.show();
    }
    
    @FXML
    private void switchToContractorMenu(ActionEvent event) throws IOException {
        switchScenes(event, "display_contractors.fxml");
    }
    
    @FXML
    private void switchToMaintenanceMenu(ActionEvent event) throws IOException {
         switchScenes(event, "display_maintenances.fxml");
    }
    
    @FXML
    private void switchToFIMenu(ActionEvent event) throws IOException {
        switchScenes(event, "display_fi.fxml");
    }
    
    @FXML
    private void closeApp() throws IOException {
        Platform.exit();
    }
}