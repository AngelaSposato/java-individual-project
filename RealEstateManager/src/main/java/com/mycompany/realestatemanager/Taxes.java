/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author angel
 */
public class Taxes {
    private double schoolTax;
    private double propertyTax;
    private double monthlyElectricity;
    private double insurance;
    private int taxID;
    public Taxes(double schoolTax, double propertyTax, double monthlyElectricity, double insurance, int taxID) {
        this.schoolTax = schoolTax;
        this.propertyTax = propertyTax;
        this.monthlyElectricity = monthlyElectricity;
        this.insurance = insurance;
        this.taxID = taxID;
    }

    public double getSchoolTax() {
        return schoolTax;
    }

    public double getPropertyTax() {
        return propertyTax;
    }

    public double getMonthlyElectricity() {
        return monthlyElectricity;
    }

    public double getInsurance() {
        return insurance;
    }
    
    public int getTaxID(){
        return taxID;
    }
}
