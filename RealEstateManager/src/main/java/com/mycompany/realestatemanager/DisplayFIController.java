package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
/**
 * 
 * @author Angela Sposato 1934695
 */
public class DisplayFIController  implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private Button addFI;

    @FXML
    private ListView<FinancialInstitution> FIListView;

    @FXML
    private Button ViewFI;

    @FXML
    private Button ReturnToMain;
    
    @FXML
    private Button removeFIButton;
    
    //initializes listView by loading FIs from DB
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ArrayList<FinancialInstitution> fiList = loadFIInfo();
            for (FinancialInstitution fi: fiList){
                FIListView.getItems().add(fi);
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    //loads FI info from db to be used in initialize()
    private ArrayList<FinancialInstitution> loadFIInfo() throws SQLException {
        ArrayList<FinancialInstitution> fis = new ArrayList<>();
        String selectQuery = "select institution_id, name, phone, interest_rate, email, website from financial_institutions";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()) {
            int institutionID = rs.getInt("institution_id");
            String name = rs.getString("name");
            String phone = rs.getString("phone");
            double interestRate = rs.getDouble("interest_rate");
            String email = rs.getString("email");
            String website = rs.getString("website");
            //creating new FI object and adding to ArrayList of type FI
            FinancialInstitution aFI = new FinancialInstitution(institutionID, name, phone, interestRate, email, website);
            fis.add(aFI);
        }
        return fis;
    }
    
    //Removes FI from ListView
    @FXML 
    public void removeFinancialInstitution(ActionEvent event) throws SQLException {
        int selectedID = FIListView.getSelectionModel().getSelectedIndex();
        removeFIFromDB();
        FIListView.getItems().remove(selectedID);
    }
    
    //Removes FI from DB
    private void removeFIFromDB() throws SQLException {
        try {
            //gets selected item
            FinancialInstitution fiToRemove = FIListView.getSelectionModel().getSelectedItem();
            int fiID = fiToRemove.getInstitutionID();
            
            //removing based on unique identifier
             String removeQuery = "DELETE FROM financial_institutions WHERE institution_id =" + fiID; 
            PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //These methods switch views
    @FXML 
    void switchScenes(ActionEvent event, String filename) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource(filename));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    public void switchToAddFI(ActionEvent event) throws IOException {
        switchScenes(event, "add_fi.fxml");
    }

    @FXML
    public void switchToMainMenu(ActionEvent event) throws IOException {
        switchScenes(event, "main_menu_final.fxml");
    }

    @FXML
    public void switchToViewFI(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("view_fi.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        FIViewController controller = fxmlLoader.getController();
        controller.initData(FIListView.getSelectionModel().getSelectedItem());
       
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

  
}