package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.Contractor;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class DisplayContractorController implements Initializable{
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private Button addContractor;

    @FXML
    private ListView<Contractor> contractorListView;

    @FXML
    private Button goBack;

    @FXML
    private Button ViewContractor;
    
    @FXML
    private Button removeContractorButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ArrayList<Contractor> contractorList = loadContractorInfo();
            for (Contractor contractor : contractorList) {
                contractorListView.getItems().add(contractor);
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    //Loads contractor info from DB to display in ListView 
    private ArrayList<Contractor> loadContractorInfo() throws SQLException {
        ArrayList<Contractor> contractors = new  ArrayList<>();
        String selectQuery = "select contractor_id, first_name, last_name, phone_number, email, trade, enterprise_name, additional_comments from contractor";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int contractorID = rs.getInt("contractor_id");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String phoneNumber = rs.getString("phone_number");
            String email = rs.getString("email");
            String trade = rs.getString("trade");
            String entName = rs.getString("enterprise_name");
            String additionalComments = rs.getString("additional_comments");
            Contractor aContractor = new Contractor(contractorID, firstName, lastName, phoneNumber, email, trade, entName, additionalComments);
            contractors.add(aContractor);
        }
        return contractors;
    }
    
    //Removes from ListView and calls method to remove from DB
    @FXML
    private void removeContractor(ActionEvent event) throws SQLException {
        removeContractorFromDB();
        int selectedID = contractorListView.getSelectionModel().getSelectedIndex();
        contractorListView.getItems().remove(selectedID);
    }
    
    //Removes from DB using unique identifier
    private void removeContractorFromDB() throws SQLException {
        try {
            Contractor contractorToRemove = contractorListView.getSelectionModel().getSelectedItem();
            int contractorID = contractorToRemove.getContractorID();
            String removeQuery = "DELETE FROM CONTRACTOR where contractor_id ="+ contractorID;
            PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //These methods switch scenes
    
    @FXML 
    void switchScenes(ActionEvent event, String filename) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource(filename));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    void switchToAddContractor(ActionEvent event) throws IOException {
       switchScenes(event, "add_contractors.fxml");
    }

    @FXML
    void switchToMainMenu(ActionEvent event) throws IOException {
        switchScenes(event, "main_menu_final.fxml");
    }

    @FXML
    void switchToViewContractor(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("view_contractor.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        ContractorViewController controller = fxmlLoader.getController();
        controller.initData(contractorListView.getSelectionModel().getSelectedItem());
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
}
