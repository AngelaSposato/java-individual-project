package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class DisplayMaintenanceController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private Button addMaintenance;

    @FXML
    private ListView<Maintenance> maintenanceListView;

    @FXML
    private Button ViewMaintenance;

    @FXML
    private Button returnToMain;
    
    @FXML
    private Button removeMaintenanceButton;
    
    //Initializes ListView of maintenances
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ArrayList<Maintenance> maintenanceList = loadMaintenanceInfo();
            for (Maintenance maintenance: maintenanceList) {
                maintenanceListView.getItems().add(maintenance);
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    //Loads maintenances from database
    public ArrayList<Maintenance> loadMaintenanceInfo() throws SQLException {
        ArrayList<Maintenance> maintenances = new ArrayList<>();
        String selectQuery = "select maintenance_id, maintenance_name, address, unit, contractor_name, contractor_type, cost, maintenance_date, mark_as_complete from maintenances";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            int maintenanceID = rs.getInt("maintenance_id");
            String maintenanceName = rs.getString("maintenance_name");
            String address = rs.getString("address");
            String unit = rs.getString("unit");
            String contractorName = rs.getString("contractor_name");
            String contractorType = rs.getString("contractor_type");
            int cost = rs.getInt("cost");
            String maintenanceDate = rs.getString("maintenance_date");
            boolean markAsComplete = rs.getBoolean("mark_as_complete");
            Maintenance aMaintenance = new Maintenance(maintenanceID, maintenanceName, address, unit, contractorName, contractorType, cost, maintenanceDate, markAsComplete);
            maintenances.add(aMaintenance);
        }
        return maintenances;
    }
    
    //Removes selected maintenance from ListView and calls method to remove from DB
    public void removeMaintenance(ActionEvent event) throws SQLException {
        int selectedID = maintenanceListView.getSelectionModel().getSelectedIndex();
        removeMaintenanceFromDB();
        maintenanceListView.getItems().remove(selectedID);
    }
    
    //Removes selected maintenance from DB using unique identifier
    private void removeMaintenanceFromDB() throws SQLException {
        try {
            Maintenance maintenanceToRemove = maintenanceListView.getSelectionModel().getSelectedItem();
            int maintenanceID = maintenanceToRemove.getMaintenanceID();
            String removeQuery = "DELETE FROM MAINTENANCES WHERE MAINTENANCE_ID=" + maintenanceID;
            PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //These methods switch scenes
    @FXML 
    void switchScenes(ActionEvent event, String filename) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource(filename));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    void switchToMainMenu(ActionEvent event) throws IOException {
        switchScenes(event, "main_menu_final.fxml");
    }
    
    @FXML
    void switchToAddMaintenance(ActionEvent event) throws IOException {
        switchScenes(event, "add_maintenance.fxml");
    }


    @FXML
    void switchToViewMaintenance(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("view_maintenance.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        MaintenanceViewController controller = fxmlLoader.getController();
        controller.initData(maintenanceListView.getSelectionModel().getSelectedItem());
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

}
