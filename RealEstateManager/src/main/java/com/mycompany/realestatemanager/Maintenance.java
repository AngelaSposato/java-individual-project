/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author Angela Sposato 1934695
 */
public class Maintenance {
    
    private int maintenanceID;
    private String maintenanceName;
    private String address;
    private String unit;
    private String contractorName;
    private String contractorType;
    private int cost;
    private String date;
    private boolean markAsComplete;

    public Maintenance(int maintenanceID, String maintenanceName, String address, String unit, String contractorName, String contractorType, int cost, String date, boolean markAsComplete) {
        this.maintenanceID = maintenanceID;
        this.maintenanceName = maintenanceName;
        this.address = address;
        this.unit = unit;
        this.contractorName = contractorName;
        this.contractorType = contractorType;
        this.cost = cost;
        this.date = date;
        this.markAsComplete = markAsComplete;
    }
    
    public int getMaintenanceID(){
        return maintenanceID;
    }

    public String getMaintenanceName() {
        return maintenanceName;
    }

    public String getAddress() {
        return address;
    }

    public String getUnit() {
        return unit;
    }

    public String getContractorName() {
        return contractorName;
    }

    public String getContractorType() {
        return contractorType;
    }

    public int getCost() {
        return cost;
    }

    public String getDate() {
        return date;
    }

    public boolean isMarkAsComplete() {
        return markAsComplete;
    }

    @Override
    public String toString() {
        return maintenanceName;
    }

}
