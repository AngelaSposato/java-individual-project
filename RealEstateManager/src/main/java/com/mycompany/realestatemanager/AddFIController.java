package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddFIController {
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private TextField nameText;

    @FXML
    private TextField phoneText;

    @FXML
    private TextField interestRateText;

    @FXML
    private TextField emailAddressText;

    @FXML
    private TextField websiteText;

    @FXML
    private Button CancelButton;

    @FXML
    private Button ConfirmAddFI;
    
    private int getNewFinancialInstitutionID() throws SQLException {
        String query = "select institution_id from financial_institutions where institution_id=(select max(institution_id) from financial_institutions)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestFI = 0;
        while(results.next()){
            highestFI = results.getInt("institution_id");
            highestFI++;
        }
        return highestFI;
    } 

    @FXML 
    void addFI(ActionEvent event) throws IOException, SQLException {
        //Creating new FI object
        FinancialInstitution fi = new FinancialInstitution(getNewFinancialInstitutionID(), nameText.getText(), phoneText.getText(), Double.parseDouble(interestRateText.getText()), emailAddressText.getText(), websiteText.getText());
        
        //Inserting into DB
        addFIToDB(fi);
        
        //GO BACK
        switchToDisplayFIs(event);
        
    }
    
    void addFIToDB(FinancialInstitution fi) throws SQLException {
        try {
            String query="insert into financial_institutions(name, phone, interest_rate, email, website) values (?, ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, fi.getName());
            ps.setString(2, fi.getPhone());
            ps.setDouble(3, fi.getInterestRate());
            ps.setString(4, fi.getEmail());
            ps.setString(5, fi.getWebsite());
            ps.execute();
            System.out.println("Financial Institution successfully added to database"); 
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
    @FXML
    void switchToDisplayFIs(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_fi.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

}
