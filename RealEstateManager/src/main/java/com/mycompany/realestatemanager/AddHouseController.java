package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela
 */
public class AddHouseController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    ObservableList<String> financialInstitutions;
    
    @FXML
    private TextField schoolTax;

    @FXML
    private TextField propertyTax;

    @FXML
    private TextField Insurance;

    @FXML
    private TextField monthlyElectricity;

    @FXML
    private TextField addressText;

    @FXML
    private TextField postalCodeText;

    @FXML
    private TextField mortgageAmount;

    @FXML
    private ChoiceBox<String> financialInstitutionCB;

    @FXML
    private TextField currMonthlyPayment;

    @FXML
    private DatePicker renewalDate;

    @FXML
    private Button cancelButton;

    @FXML
    private Button confirmButton;
    
    @FXML 
    void addHouse(ActionEvent event) throws IOException, SQLException {
        //Creating objects for property
        House newHouse = new House(getNewPropertyID(), addressText.getText(), postalCodeText.getText(), "House");
        Mortgage newMortgage = new Mortgage(Double.parseDouble(mortgageAmount.getText()), Double.parseDouble(currMonthlyPayment.getText()), financialInstitutionCB.getValue(), renewalDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), newHouse.getPropertyID());
        Taxes newTaxes = new Taxes(Double.parseDouble(schoolTax.getText()),  Double.parseDouble(propertyTax.getText()), Double.parseDouble(monthlyElectricity.getText()), Double.parseDouble(Insurance.getText()), newHouse.getPropertyID());
        
        //Using new objects to insert into DB
        addHouseToDB(newHouse);
        addMortgageToDB(newMortgage, newHouse);
        addTaxesToDB(newHouse, newTaxes);
        
        //GO BACK
       switchToDisplayProperty(event);
    }
    
    //Adds house to property table in database
     private void addHouseToDB(House newProperty) throws SQLException {
        try {
            //Database auto-generates a primary key
            String query="insert into property(address, postal_code, property_type) values (?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, newProperty.getAddress());
            ps.setString(2, newProperty.getPostalCode());
            ps.setString(3, newProperty.getPropertyType());
            ps.execute();
            System.out.println("Property successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //Adds mortgage to mortgages table in DB using property_id as foreign key
    private void addMortgageToDB(Mortgage newMortgage, House newProperty) throws SQLException {
        try {
            String query="insert into mortgage(property_id, total_amount, monthly_payment, financial_institution, renewal_date) values ((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newMortgage.getTotalAmount());
            ps.setDouble(2, newMortgage.getMonthlyPayment());
            ps.setString(3, newMortgage.getFinancialInstitution());
            ps.setString(4, newMortgage.getRenewalDate());
            ps.execute();
            System.out.println("Mortgage successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Adds to taxes_utilities table using property_id as foreign key
    private void addTaxesToDB(House newProperty, Taxes newTaxes) throws SQLException {
        try {
            String query = "insert into taxes_utilities(property_id, school_tax, property_tax, monthly_electricity, insurance) values((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newTaxes.getSchoolTax());
            ps.setDouble(2, newTaxes.getPropertyTax());
            ps.setDouble(3, newTaxes.getMonthlyElectricity());
            ps.setDouble(4, newTaxes.getInsurance());
            ps.execute();
            System.out.println("Tax info successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //gets a new Property ID by getting the highest one in Database and increments by 1
    int getNewPropertyID() throws SQLException {
        String query = "select property_id from property where property_id=(select max(property_id) from property)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestPropertyID = 0;
        while(results.next()){
            highestPropertyID = results.getInt("property_id");
            highestPropertyID++;
        }
        return highestPropertyID;
    }
    
    //switches to display properties view
    @FXML
    void switchToDisplayProperty(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_properties.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        financialInstitutions =  FXCollections.observableArrayList();
        financialInstitutions.addAll("CIBC", "RBC", "TD", "BMO");
        financialInstitutionCB.setItems(financialInstitutions);
    }

}