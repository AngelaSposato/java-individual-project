package com.mycompany.realestatemanager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class ChoosePropertyTypeController implements Initializable{

    ObservableList<String> propertyTypeList;
    
    @FXML
    private Button cancelButton;

    @FXML
    private Button confirmButton;

    @FXML
    private ChoiceBox<String> PropertyTypeCB;

    @FXML
    void switchToDisplayProperty(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_properties.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
        /*FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("display_properties.fxml"));
        Parent parent = fxmlLoader.load();
        App.setRoot(parent);*/
    }
    
    //Depending on what user puts in dropdown menu it will change scenes to add appropriate property
    @FXML
    void switchToAddProperty(ActionEvent event) throws IOException {
        if (PropertyTypeCB.getValue().equals("Condo")){
            Parent listViewParent = FXMLLoader.load(getClass().getResource("add_condo.fxml"));
            Scene listViewScene = new Scene(listViewParent);
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
            window.setScene(listViewScene);
            window.show();
            /*FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("add_condo.fxml"));
            Parent parent = fxmlLoader.load();
            App.setRoot(parent);*/
        }
        else if (PropertyTypeCB.getValue().equals("House")){
            Parent listViewParent = FXMLLoader.load(getClass().getResource("add_house.fxml"));
            Scene listViewScene = new Scene(listViewParent);
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

            window.setScene(listViewScene);
            window.show();
            /*FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("add_house.fxml"));
            Parent parent = fxmlLoader.load();
            App.setRoot(parent);*/
        }
        else if (PropertyTypeCB.getValue().equals("Plex")) {
            Parent listViewParent = FXMLLoader.load(getClass().getResource("add_plex.fxml"));
            Scene listViewScene = new Scene(listViewParent);
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
            window.setScene(listViewScene);
            window.show();
            /*FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("add_plex.fxml"));
            Parent parent = fxmlLoader.load();
            App.setRoot(parent);*/
        }
    }
    
    //Initializes dropdown menu
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        propertyTypeList = FXCollections.observableArrayList();
        propertyTypeList.addAll("House", "Condo", "Plex");
        PropertyTypeCB.setItems(propertyTypeList);
    }

}