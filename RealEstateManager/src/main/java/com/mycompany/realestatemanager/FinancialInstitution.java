/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author Angela Sposato 1934695
 */
public class FinancialInstitution {
    private int financialInstitutionID;
    private String name;
    private String phone;
    private double interestRate;
    private String email;
    private String website;

    public FinancialInstitution(int financialInstitutionID, String name, String phone, double interestRate, String email, String website) {
        this.financialInstitutionID = financialInstitutionID;
        this.name = name;
        this.phone = phone;
        this.interestRate = interestRate;
        this.email = email;
        this.website = website;
    }
    public int getInstitutionID(){
        return financialInstitutionID;
}
    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public double getInterestRate() {
        return interestRate;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getWebsite(){
        return website;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
