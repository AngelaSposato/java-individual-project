package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddContractorController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    private Parent back;
    
    ObservableList<String> contractorTypes;
    
    @FXML
    private TextField firstNameText;

    @FXML
    private TextField lastNameText;

    @FXML
    private TextField phoneNumText;

    @FXML
    private TextField emailText;

    @FXML
    private ChoiceBox<String> tradeChoiceBox;

    @FXML
    private TextField entText;

    @FXML
    private Button CancelButton;

    @FXML
    private Button confirmButton;
    
    @FXML
    private TextArea additionalComments;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       contractorTypes = FXCollections.observableArrayList();
       contractorTypes.addAll("Electrician", "Plumber", "Painter", "HVAC", "General Contractor", "Exterminator");
       tradeChoiceBox.setItems(contractorTypes);
       tradeChoiceBox.setValue("Electrician");
    }
    
    //Gets highest contractor ID and increments to make a new tenant ID 
    private int getNewContractorID() throws SQLException {
        String query = "select contractor_id from contractor where contractor_id=(select max(contractor_id) from contractor)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestContractorID = 0;
        while(results.next()){
            highestContractorID = results.getInt("contractor_id");
            highestContractorID++;
        }
        return highestContractorID;
    }
    
    public void addContractor(ActionEvent event) throws IOException, SQLException {
        //Creating objects for contractor
        Contractor newContractor = new Contractor(getNewContractorID(), firstNameText.getText(), lastNameText.getText(), phoneNumText.getText(), emailText.getText(), tradeChoiceBox.getValue(), entText.getText(), additionalComments.getText());
        System.out.println(newContractor);
        
        //Using objects to insert into DB
        addContractorToDB(newContractor);
        
        //GO BACK
        switchToDisplayContractors(event);
    }
    
    private void addContractorToDB(Contractor newContractor) throws SQLException {
        try {
            String query = "insert into contractor(first_name, last_name, phone_number, email, trade, enterprise_name, additional_comments) values (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, newContractor.getFirstName());
            ps.setString(2, newContractor.getLastName());
            ps.setString(3, newContractor.getPhone());
            ps.setString(4, newContractor.getEmail());
            ps.setString(5, newContractor.getTrade());
            ps.setString(6, newContractor.getEnterprise());
            ps.setString(7, newContractor.getAdditionalComments());
            ps.execute();
            System.out.println("Contractor successfully inserted into database");
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    @FXML
    void switchToDisplayContractors(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_contractors.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
}
