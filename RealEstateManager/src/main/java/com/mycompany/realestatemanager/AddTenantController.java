package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.File;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddTenantController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    private Parent back;
    
    @FXML
    private ListView<Tenant> tenantView;
    
    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField phoneNum;

    @FXML
    private TextField email;

    @FXML
    private TextField addrRented;

    @FXML
    private TextField unitRented;

    @FXML
    private DatePicker leaseRenewal;

    @FXML
    private TextField currentRent;

    @FXML
    private Button CancelButton;

    @FXML
    private Button ConfirmButton;

    @FXML
    private TextArea addComments;
    
    @FXML
    private Button attachLease;
    
    @FXML 
    private ChoiceBox<Property> addressCB;
    
    ObservableList<Property> propertyList;
    ObservableList<Contractor> contractorList;
    ObservableList<String> typeList;
    
    @FXML
    void attachLease(ActionEvent event) throws IOException {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("PDF Files", "*.pdf"));
        File selectedFile = fc.showOpenDialog(null);
        if (selectedFile != null){
            //create new directory for this specific tenant
            new File("C:\\Users\\angel\\Documents\\School\\Semester 4\\Java\\NetBeans\\java-individual-project\\RealEstateManager\\leases\\"+ firstName.getText()+lastName.getText()).mkdir();
            Path from = Paths.get(selectedFile.toURI());
            Path to = Paths.get("C:\\Users\\angel\\Documents\\School\\Semester 4\\Java\\NetBeans\\java-individual-project\\RealEstateManager\\leases\\"+firstName.getText()+lastName.getText()+"\\"+selectedFile.getName());
            //copies file to new directory and renames file to follow naming convention
            Files.copy(from, to.resolveSibling(firstName.getText()+lastName.getText()+"Lease.pdf"));
        }
    }
    
    //Gets highest tenant ID and increments to make a new tenant ID 
   private int getNewTenantID() throws SQLException {
        String query = "select tenant_id from tenant where tenant_id=(select max(tenant_id) from tenant)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestTenantID = 0;
        while(results.next()){
            highestTenantID = results.getInt("tenant_id");
            highestTenantID++;
        }
        return highestTenantID;
    }
    
    
    @FXML
    //Adds tenant to ListView        
    private void addTenant(ActionEvent event) throws IOException, SQLException {
        Tenant newTenant = new Tenant(getNewTenantID(), firstName.getText(), lastName.getText(), email.getText(), phoneNum.getText(), addressCB.getValue().toString(), unitRented.getText(), parseDouble(currentRent.getText()), leaseRenewal.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), addComments.getText());
        System.out.println("Successfully created new tenant");
        addTenantToDB(newTenant);
        //tenantView.getItems().add(newTenant);
        switchToDisplayTenants(event);
    }
    
    //Adds tenant to DB
    private void addTenantToDB(Tenant newTenant) throws SQLException {
        try {
            String query="insert into tenant(tenant_id, first_name, last_name, email, phone, address_rented, unit_rented, monthly_rent, next_lease_renewal, additional_comments) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps =  instance.getConnection().prepareStatement(query);
            ps.setInt(1, newTenant.getTenantID());
            ps.setString(2, newTenant.getFirstName());
            ps.setString(3, newTenant.getLastName());
            ps.setString(4, newTenant.getEmail());
            ps.setString(5, newTenant.getPhone());
            ps.setString(6, newTenant.getAddressRented());
            ps.setString(7, newTenant.getUnitRented());
            ps.setDouble(8, newTenant.getMonthlyRent());
            ps.setString(9, newTenant.getRenewalDate());
            ps.setString(10, newTenant.getAdditionalComments());
            ps.execute();
            ps.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    void setListView(ListView<Tenant> tenant){
        tenantView = tenant;
    }
    
    void setParent(Parent back) {
        this.back = back;
    }
    
    @FXML
    void switchToDisplayTenants(ActionEvent event) throws IOException{
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_tenants.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public ArrayList<Property> loadPropertyInfo() throws SQLException {
        ArrayList<Property> properties = new ArrayList<>();
        String selectQuery="select property_id, address, postal_code, number_units, property_type from property";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            int propertyID = rs.getInt("property_id");
            String address = rs.getString("address");
            String postalCode = rs.getString("postal_code");
            String numberUnits = rs.getString("number_units");
            String propertyType = rs.getString("property_type");
            Property aProperty = new Property(propertyID, address, postalCode, propertyType);
            properties.add(aProperty);
        }
        return properties;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
         propertyList = FXCollections.observableArrayList();
        try {
            propertyList.addAll(loadPropertyInfo());
        } catch (SQLException ex) {
            Logger.getLogger(AddTenantController.class.getName()).log(Level.SEVERE, null, ex);
        }
         addressCB.setItems(propertyList);
    }

}
