package com.mycompany.realestatemanager;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;


/**
 * 
 * @author Angela Sposato 1934695
 */
public class FIViewController {
    private FinancialInstitution selectedFI;
    
    @FXML
    private Label websiteLabel;

    @FXML
    private Label interestLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private Button returnToDisplay;

    @FXML
    private Label nameLabel;

    @FXML
    private Label phoneLabel;

    @FXML
    void switchToDisplayFIs(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_fi.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public void initData(FinancialInstitution fi){
        selectedFI = fi;
        nameLabel.setText(selectedFI.getName());
        interestLabel.setText(selectedFI.getInterestRate()+"%");
        phoneLabel.setText(selectedFI.getPhone());
        emailLabel.setText(selectedFI.getEmail());
        websiteLabel.setText(selectedFI.getWebsite());
        
    }
}
