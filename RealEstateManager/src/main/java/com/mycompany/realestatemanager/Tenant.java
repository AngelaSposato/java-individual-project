/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author Angela Sposato 1934695
 */
public class Tenant {
    private int tenantID;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String addressRented;
    private String unitRented;
    private double monthlyRent;
    private String nextRenewalDate;
    private String additionalComments;
    
    public Tenant(int tenantID, String firstName, String lastName, String email, String phone, String addressRented, String unitRented, double monthlyRent, String nextRenewalDate, String additionalComments) {
        this.tenantID=tenantID;
        this.firstName= firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.addressRented = addressRented;
        this.unitRented = unitRented;
        this.monthlyRent = monthlyRent;
        this.nextRenewalDate = nextRenewalDate;
        this.additionalComments = additionalComments;
    }
    
    public int getTenantID(){
        return tenantID;
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getLastName(){
        return lastName;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getPhone(){
        return phone;
    }
    
    public String getAddressRented(){
        return addressRented;
    }
   
    public String getUnitRented(){
        return unitRented;
    }
    
    public double getMonthlyRent(){
        return monthlyRent;
    }
    
    public String getRenewalDate(){
        return nextRenewalDate;
    }
    
    public String getAdditionalComments(){
        return additionalComments;
    }
    
    @Override
    public String toString(){
        return firstName + " " + lastName;
    }
    
}
