/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author angel
 */
public class Condo extends Property {
    private double condoFees;
    
    public Condo(double condoFees, int propertyID, String address, String postalCode, String propertyType) {
        super(propertyID, address, postalCode, propertyType);
        this.condoFees = condoFees;
    }
    
    public double getCondoFees(){
        return condoFees;
    }
}
