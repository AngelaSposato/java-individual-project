/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.Property;

/**
 *
 * @author angel
 */
public class Plex extends Property {
    private String numberUnits;
    
    public Plex(String numberUnits, int propertyID, String address, String postalCode, String propertyType) {
        super(propertyID, address, postalCode, propertyType);
        this.numberUnits = numberUnits;
    }
    
    public String getNumberUnits(){
        return numberUnits;
    }
}
