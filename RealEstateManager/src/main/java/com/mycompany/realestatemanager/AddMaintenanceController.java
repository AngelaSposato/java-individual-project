package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import com.mycompany.realestatemanager.Contractor;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddMaintenanceController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    private Parent back;
    
    ObservableList<Property> propertyList;
    ObservableList<Contractor> contractorList;
    ObservableList<String> typeList;
    
    @FXML
    private TextField unitText;

    @FXML
    private TextField costText;

    @FXML
    private DatePicker dateBox;

    @FXML
    private CheckBox markAsComplete;

    @FXML
    private TextField maintenanceNameText;

    @FXML
    private ChoiceBox<Contractor> contractorCB;

    @FXML
    private ChoiceBox<String> typeCB;

    @FXML
    private ChoiceBox<Property> addressCB;

    @FXML
    private Button CancelButton;

    @FXML
    private Button ConfirmAdd;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            //loading choice box of properties
            propertyList = FXCollections.observableArrayList();
            propertyList.addAll(loadPropertyInfo());
            addressCB.setItems(propertyList);
            
            //loading ChoiceBox of Contractor
            contractorList = FXCollections.observableArrayList();
            contractorList.addAll(loadContractorInfo());
            contractorCB.setItems(contractorList);
            
            //loading ChoiceBox of Type
            typeList = FXCollections.observableArrayList();
            typeList.addAll("Painting", "Plumbing", "Electrical", "General", "HVAC", "Pest Removal");
            typeCB.setItems(typeList);
        } catch (SQLException ex) {
            Logger.getLogger(AddMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    private int getNewMaintenanceID() throws SQLException {
        String query = "select maintenance_id from maintenances where maintenance_id=(select max(maintenance_id) from maintenances)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestMaintenanceID = 0;
        while(results.next()){
            highestMaintenanceID = results.getInt("maintenance_id");
            highestMaintenanceID++;
        }
        return highestMaintenanceID;
    }
    
    @FXML
    void addMaintenance(ActionEvent event) throws IOException, SQLException {
        //Creating new maintenance object
        Maintenance newMaintenance = new Maintenance(getNewMaintenanceID(), maintenanceNameText.getText(), addressCB.getValue().toString(), unitText.getText(), contractorCB.getValue().toString(), typeCB.getValue(), Integer.parseInt(costText.getText()), dateBox.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), false);
        System.out.println(newMaintenance);
        
        //Inserting into DB
        addMaintenanceToDB(newMaintenance);
        
        
        //GO BACK
        switchToDisplayMaintenances(event);
    }
    
    void addMaintenanceToDB(Maintenance newMaintenance) throws SQLException {
        try {
            String query="insert into maintenances(maintenance_name, address, unit, contractor_name, contractor_type, cost, maintenance_date, mark_as_complete) values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, newMaintenance.getMaintenanceName());
            ps.setString(2, newMaintenance.getAddress());
            ps.setString(3, newMaintenance.getUnit());
            ps.setString(4, newMaintenance.getContractorName());
            ps.setString(5, newMaintenance.getContractorType());
            ps.setInt(6, newMaintenance.getCost());
            ps.setString(7, newMaintenance.getDate());
            ps.setBoolean(8, newMaintenance.isMarkAsComplete());
            ps.execute();
            System.out.println("Maintenance successfully inserted into database");
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public ArrayList<Property> loadPropertyInfo() throws SQLException {
        ArrayList<Property> properties = new ArrayList<>();
        String selectQuery="select property_id, address, postal_code, number_units, property_type from property";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            int propertyID = rs.getInt("property_id");
            String address = rs.getString("address");
            String postalCode = rs.getString("postal_code");
            String numberUnits = rs.getString("number_units");
            String propertyType = rs.getString("property_type");
            Property aProperty = new Property(propertyID, address, postalCode, propertyType);
            properties.add(aProperty);
        }
        return properties;
    }
    
    public ArrayList<Contractor> loadContractorInfo() throws SQLException {
        ArrayList<Contractor> contractors = new ArrayList<>();
        String selectQuery = "select contractor_id, first_name, last_name, phone_number, email, trade, enterprise_name, additional_comments from contractor";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int contractorID = rs.getInt("contractor_id");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String phoneNumber = rs.getString("phone_number");
            String email = rs.getString("email");
            String trade = rs.getString("trade");
            String entName = rs.getString("enterprise_name");
            String additionalComments = rs.getString("additional_comments");
            Contractor aContractor = new Contractor(contractorID, firstName, lastName, phoneNumber, email, trade, entName, additionalComments);
            contractors.add(aContractor);
        }
        return contractors;
    }
    
    @FXML
    void switchToDisplayMaintenances(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_maintenances.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

}
