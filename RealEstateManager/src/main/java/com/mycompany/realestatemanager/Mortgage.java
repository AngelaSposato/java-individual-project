/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author Angela Sposato 1934695
 */
public class Mortgage {
    private int mortgageID;
    private double totalAmount;
    private double monthlyPayment; 
    private String financialInstitution;
    private String renewalDate;
    
    public Mortgage(double totalAmount, double monthlyPayment, String financialInstitution, String renewalDate, int mortgageID){
        this.totalAmount = totalAmount;
        this.monthlyPayment = monthlyPayment;
        this.financialInstitution = financialInstitution;
        this.renewalDate = renewalDate;
        this.mortgageID = mortgageID;
    }
    
    public double getTotalAmount(){
        return totalAmount;
    }
    
    public double getMonthlyPayment(){
        return monthlyPayment;
    }
    
    public String getFinancialInstitution(){
        return financialInstitution;
    }
    
    public String getRenewalDate(){
        return renewalDate;
    }
    
    public int getMortgageID(){
        return mortgageID;
    }
    
    @Override
    public String toString(){
        return totalAmount + " " + monthlyPayment + " " + financialInstitution + " " + renewalDate;
    }
}
