package com.mycompany.realestatemanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *This class is a singleton for database Connection.
 * @author Angela Sposato 1934695
 */
public class DBConnection {
    
    private static DBConnection instance = new DBConnection();
    private Connection connect;
    private String url = "jdbc:mysql://localhost:3306/real_estate_manager";
    private String user = "root";
    private String password = "Compsci420!@";

    private DBConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connect= DriverManager.getConnection(url, user, password);
            System.out.println("Connection is successful to db" + url);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public Connection getConnection(){
        return connect;
    }
    
    public static DBConnection getInstance(){
        if (instance == null)
            instance = new DBConnection();
        return instance;
    }
}
