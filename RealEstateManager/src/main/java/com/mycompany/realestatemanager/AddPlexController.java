package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddPlexController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    ObservableList<String> financialInstitutions;
    
    @FXML
    private TextField schoolTax;

    @FXML
    private TextField propertyTax;

    @FXML
    private TextField Insurance;

    @FXML
    private TextField monthlyElectricity;

    @FXML
    private TextField addressText;

    @FXML
    private TextField postalCodeText;

    @FXML
    private TextField numUnitsText;

    @FXML
    private TextField mortgageAmount;

    @FXML
    private ChoiceBox<String> financialInstitutionCB;

    @FXML
    private TextField currMonthlyPayment;

    @FXML
    private DatePicker renewalDate;

    @FXML
    private Button cancelButton;

    @FXML
    private Button confirmButton;

    @FXML 
    void addPlex(ActionEvent event) throws IOException, SQLException {
        //Creating objects for property
        Plex newPlex = new Plex(numUnitsText.getText(), getNewPropertyID(), addressText.getText(), postalCodeText.getText(), "Plex");
        Mortgage newMortgage = new Mortgage(Double.parseDouble(mortgageAmount.getText()), Double.parseDouble(currMonthlyPayment.getText()), financialInstitutionCB.getValue(), renewalDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), newPlex.getPropertyID());
        Taxes newTaxes = new Taxes(Double.parseDouble(schoolTax.getText()),  Double.parseDouble(propertyTax.getText()), Double.parseDouble(monthlyElectricity.getText()), Double.parseDouble(Insurance.getText()), newPlex.getPropertyID());
        
        //Using new objects to insert into DB
        addPlexToDB(newPlex);
        addMortgageToDB(newMortgage, newPlex);
        addTaxesToDB(newPlex, newTaxes);
        
        //GO BACK
        switchToDisplayProperty(event);
    }
    
    //Adds to properties table
    private void addPlexToDB(Plex newProperty) throws SQLException {
        try {
            String query="insert into property(address, postal_code, number_units, property_type) values (?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, newProperty.getAddress());
            ps.setString(2, newProperty.getPostalCode());
            ps.setString(3, newProperty.getNumberUnits());
            ps.setString(4, newProperty.getPropertyType());
            ps.execute();
            //instance.getConnection().close();
            System.out.println("Property successfully inserted into database");
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Adds to mortgage table with foreign key of property
     private void addMortgageToDB(Mortgage newMortgage, Plex newProperty) throws SQLException {
        try {
            String query="insert into mortgage(property_id, total_amount, monthly_payment, financial_institution, renewal_date) values ((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newMortgage.getTotalAmount());
            ps.setDouble(2, newMortgage.getMonthlyPayment());
            ps.setString(3, newMortgage.getFinancialInstitution());
            ps.setString(4, newMortgage.getRenewalDate());
            ps.execute();
            System.out.println("Mortgage successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     //Gets highest tenant ID and increments to make a new tenant ID 
    int getNewPropertyID() throws SQLException {
        String query = "select property_id from property where property_id=(select max(property_id) from property)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestPropertyID = 0;
        while(results.next()){
            highestPropertyID = results.getInt("property_id");
            highestPropertyID++;
        }
        return highestPropertyID;
    }
    
    //Adds to taxes_utilities table in DB
    private void addTaxesToDB(Plex newProperty, Taxes newTaxes) throws SQLException {
        try {
            String query = "insert into taxes_utilities(property_id, school_tax, property_tax, monthly_electricity, insurance) values((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newTaxes.getSchoolTax());
            ps.setDouble(2, newTaxes.getPropertyTax());
            ps.setDouble(3, newTaxes.getMonthlyElectricity());
            ps.setDouble(4, newTaxes.getInsurance());
            ps.execute();
            System.out.println("Tax info successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Switches to display properties view
    @FXML
    void switchToDisplayProperty(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_properties.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
      financialInstitutions =  FXCollections.observableArrayList();
      financialInstitutions.addAll("CIBC", "RBC", "TD", "BMO");
      financialInstitutionCB.setItems(financialInstitutions);
    }
}
