package com.mycompany.realestatemanager;
/**
 * 
 * @author Angela Sposato 1934695
 */
public class Contractor {
    private int contractorID;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String trade;
    private String enterpriseName;
    private String additionalComments;
    
    public Contractor(int contractorID, String firstName, String lastName, String phoneNumber, String email, String trade, String enterpriseName, String additionalComments){
        this.contractorID = contractorID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.trade = trade;
        this.enterpriseName = enterpriseName;
        this.additionalComments = additionalComments;
    }
    public int getContractorID(){
        return contractorID;
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getLastName(){
        return lastName;
    }
    
    public String getPhone(){
        return phoneNumber;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getTrade(){
        return trade;
    }
    
    public String getEnterprise(){
        return enterpriseName;
    }
    
    public String getAdditionalComments(){
        return additionalComments;
    }
    
    @Override
    public String toString(){
        return firstName + " " + lastName;
    }
}
