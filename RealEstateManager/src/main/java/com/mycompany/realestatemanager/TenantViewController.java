package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.Tenant;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author angel
 */
public class TenantViewController {
    private Tenant selectedTenant;
    
    @FXML
    private Label addressBox;

    @FXML
    private Label UnitBox;

    @FXML
    private Label dateBox;

    @FXML
    private Label rentBox;

    @FXML
    private Button ReturnToDisplayTenant;

    @FXML
    private Label nameLabel;

    @FXML
    private Label phoneLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private Button viewLease;
    
    @FXML
    private TextArea additionalComments;
    
    //HOST SERVICES
    private HostServices hostServices ;

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices ;
    }
    
    @FXML
    public void switchToDisplayTenants(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_tenants.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public void initData(Tenant tenant){
        selectedTenant = tenant;
        nameLabel.setText(selectedTenant.getFirstName() + " " + selectedTenant.getLastName());
        phoneLabel.setText(selectedTenant.getPhone());
        emailLabel.setText(selectedTenant.getEmail());
        addressBox.setText(selectedTenant.getAddressRented());
        UnitBox.setText(selectedTenant.getUnitRented());
        dateBox.setText(selectedTenant.getRenewalDate());
        rentBox.setText(Double.toString(selectedTenant.getMonthlyRent()));
        additionalComments.setText(selectedTenant.getAdditionalComments());
    }
    
    @FXML
    public void viewLease(ActionEvent event){
        File file = new File("C:\\Users\\angel\\Documents\\School\\Semester 4\\Java\\NetBeans\\java-individual-project\\RealEstateManager\\leases\\"+ selectedTenant.getFirstName()+selectedTenant.getLastName()+"\\"+ selectedTenant.getFirstName()+selectedTenant.getLastName()+"Lease.pdf");
        HostServices hostServices = App.getHostInstance();
        hostServices.showDocument(file.getAbsolutePath());
        System.out.println(file.getAbsolutePath());
    }
    
}
