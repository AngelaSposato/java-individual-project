package com.mycompany.realestatemanager;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * 
 * @author Angela Sposato 1934695
 */
public class MaintenanceViewController {
    private Maintenance selectedMaintenance;
    
    @FXML
    private Text addressLabel;

    @FXML
    private Button ReturnToDisplayMaintenances;

    @FXML
    private Label contractorLabel;

    @FXML
    private Label costLabel;

    @FXML
    private Label typeLabel;

    @FXML
    private Label dateLabel;

    @FXML
    private Label maintenanceNameLabel;

    @FXML
    private Text unitLabel;
    
    @FXML
    private CheckBox isComplete;
    
    @FXML
    void switchToDisplayMaintenances(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_maintenances.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public void initData(Maintenance maintenance) {
        selectedMaintenance = maintenance;
        maintenanceNameLabel.setText(selectedMaintenance.getMaintenanceName());
        addressLabel.setText(selectedMaintenance.getAddress());
        unitLabel.setText(selectedMaintenance.getUnit());
        contractorLabel.setText(selectedMaintenance.getContractorName());
        costLabel.setText(Integer.toString(selectedMaintenance.getCost()));
        typeLabel.setText(selectedMaintenance.getContractorType());
        dateLabel.setText(selectedMaintenance.getDate());
    }
}
