/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

/**
 *
 * @author Angela Sposato 
 */
public class Property {
    private int propertyID;
    private String address;
    private String postalCode;
    private String propertyType;
    public Property(int propertyID, String address, String postalCode, String propertyType){
        this.propertyID = propertyID;
        this.address = address;
        this.postalCode = postalCode;
        this.propertyType = propertyType;
    }
        
    public int getPropertyID(){
        return propertyID;
    }
    public String getAddress(){
        return address;
    }
    
    public String getPostalCode(){
        return postalCode;
    }
    
    public String getPropertyType(){
        return propertyType;
    }
    
    @Override
    public String toString(){
        return address;
    }
}
