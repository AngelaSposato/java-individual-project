package com.mycompany.realestatemanager;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class ContractorViewController {
    private Contractor selectedContractor;
    
    @FXML
    private Label typeLabel;

    @FXML
    private Label entLabel;

    @FXML
    private Label locationLabel;

    @FXML
    private Button ReturnToDisplayCon;

    @FXML
    private Label nameLabel;

    @FXML
    private Label phoneLabel;

    @FXML
    private Label emailLabel;
    
    @FXML
    private TextArea additionalComments;
    
    @FXML
    void switchToDisplayContractors(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_contractors.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public void initData(Contractor contractor) {
        selectedContractor = contractor;
        nameLabel.setText(selectedContractor.getFirstName() + " " + selectedContractor.getLastName());
        phoneLabel.setText(selectedContractor.getPhone());
        emailLabel.setText(selectedContractor.getEmail());
        typeLabel.setText(selectedContractor.getTrade());
        additionalComments.setText(selectedContractor.getAdditionalComments());
        
        
    }

}
