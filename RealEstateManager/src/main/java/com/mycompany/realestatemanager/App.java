package com.mycompany.realestatemanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.application.HostServices;

/**
 * JavaFX App
 * @author Angela Sposato 1934695
 */
public class App extends Application {
    private static HostServices hostServices;
    
    private static Scene scene;
    
    public static HostServices getHostInstance(){
        return hostServices;
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main_menu_final.fxml"));
        Parent root = loader.load();
        MainMenuController controller = loader.getController();
        controller.setHostServices(getHostServices());
        stage.setScene(new Scene(root));
        stage.show();
        hostServices = getHostServices();
        /*scene = new Scene(loadFXML("main_menu_final"), 1060, 900);
        stage.setScene(scene);
        stage.show();*/
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }
    
    public static void setRoot(Parent root) throws IOException {
        scene.setRoot(root);
    }
    
    public static Parent getRoot(){
        return scene.getRoot();
    }
    
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
    
    public static void main(String[] args) {
        launch();
    }

}