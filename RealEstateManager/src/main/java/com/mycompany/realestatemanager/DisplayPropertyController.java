package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class DisplayPropertyController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private Button addPropertyButton;

    @FXML
    private ListView<Property> propertyListView;

    @FXML
    private Button returnToMain;

    @FXML
    private Button viewProperty;
    
    @FXML
    private Button removeProperty;
    
    @Override 
    public void initialize(URL url, ResourceBundle resources) {
        try {
            ArrayList<Property> propertyList = loadPropertyInfo();
            for (Property property: propertyList){
                propertyListView.getItems().add(property);
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Loads properties from DB to display in ListView
    public ArrayList<Property> loadPropertyInfo() throws SQLException {
        ArrayList<Property> properties = new ArrayList<>();
        String selectQuery="select property_id, address, postal_code, number_units, property_type from property";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            int propertyID = rs.getInt("property_id");
            String address = rs.getString("address");
            String postalCode = rs.getString("postal_code");
            String numberUnits = rs.getString("number_units");
            String propertyType = rs.getString("property_type");
            Property aProperty = new Property(propertyID, address, postalCode, propertyType);
            properties.add(aProperty);
        }
        return properties;
    }
    
    //Removes property from ListView
    @FXML 
    void removeProperty(ActionEvent event) throws SQLException {
        removePropertyFromDB();
        removeMortgageFromDB();
        removeTaxesFromDB();
        
        int selectedID = propertyListView.getSelectionModel().getSelectedIndex();
        propertyListView.getItems().remove(selectedID);
        
    }
    //Removes property from DB
    void removePropertyFromDB() throws SQLException {
        try { 
            Property addressToRemove = propertyListView.getSelectionModel().getSelectedItem();
            int propertyID = addressToRemove.getPropertyID();
            String removeQuery = "DELETE FROM property WHERE property_id = \"" + propertyID + "\""; 
            System.out.println(removeQuery);
            PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Removes mortgage from DB
    void removeMortgageFromDB() throws SQLException {
        try {
            Property addressToRemove = propertyListView.getSelectionModel().getSelectedItem();
            int propertyID = addressToRemove.getPropertyID();
            String removeQuery = "DELETE FROM mortgage WHERE property_id = \"" + propertyID + "\"";
              PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    void removeTaxesFromDB() throws SQLException {
        try {
            Property addressToRemove = propertyListView.getSelectionModel().getSelectedItem();
            int propertyID = addressToRemove.getPropertyID();
            String removeQuery = "DELETE FROM taxes_utilities WHERE property_id = \"" + propertyID + "\"";
              PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //These methods switch scenes
    
    @FXML
    void switchToAddProperty(ActionEvent event) throws IOException{
        Parent listViewParent = FXMLLoader.load(getClass().getResource("choose_property_type.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

    @FXML
    void switchToMainMenu(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("main_menu_final.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    void switchToViewProperty(ActionEvent event) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("view_property.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        PropertyViewController controller = fxmlLoader.getController();
        controller.initData(propertyListView.getSelectionModel().getSelectedItem());
       
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    
}
