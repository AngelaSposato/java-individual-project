package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import com.mycompany.realestatemanager.AddTenantController;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class DisplayTenantController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    
    @FXML
    private Button removeTenant;

    @FXML
    private ListView<Tenant> tenantListView;

    @FXML
    private Button ReturnButton;

    @FXML
    private Button ViewTenant;
    
    private HostServices hostServices ;

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices ;
    }

     @Override 
    public void initialize(URL url, ResourceBundle resources) {
        try {
            ArrayList<Tenant> tenantList = loadTenantInfo();
            for (Tenant tenant: tenantList){
                tenantListView.getItems().add(tenant);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DisplayTenantController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Loads Tenant info from DB
    public ArrayList<Tenant> loadTenantInfo() throws SQLException {
        ArrayList<Tenant> tenants = new ArrayList<>();
        String selectQuery = "select tenant_id, first_name, last_name, email, phone, address_rented, unit_rented, monthly_rent, next_lease_renewal, additional_comments from tenant";
        PreparedStatement ps = instance.getConnection().prepareStatement(selectQuery);
        ResultSet results = ps.executeQuery();
        while (results.next()){
            int tenantID = results.getInt("tenant_id");
            String firstName = results.getString("first_name");
            String lastName = results.getString("last_name");
            String email = results.getString("email");
            String phone = results.getString("phone");
            String addressRented = results.getString("address_rented");
            String unitRented = results.getString("unit_rented");
            double monthlyRent = results.getInt("monthly_rent");
            String nextLeaseRenew = results.getString("next_lease_renewal");
            String additionalComments = results.getString("additional_comments");
            Tenant aTenant = new Tenant(tenantID, firstName, lastName, email, phone, addressRented, unitRented, monthlyRent, nextLeaseRenew, additionalComments);
            tenants.add(aTenant);
        }
        return tenants;
    }
    
    
    @FXML 
    //Removes Tenant from ListView
    void removeTenant(ActionEvent event) throws SQLException {
        int selectedID = tenantListView.getSelectionModel().getSelectedIndex();
        removeTenantFromDB();
        tenantListView.getItems().remove(selectedID);
    }
    
    //Removes Tenant from DB
    void removeTenantFromDB() throws SQLException {
        try {
            Tenant tenantToRemove = tenantListView.getSelectionModel().getSelectedItem();
            int tenantID = tenantToRemove.getTenantID();
            String removeQuery = "DELETE FROM tenant WHERE tenant_id =" + tenantID; 
            PreparedStatement ps = instance.getConnection().prepareStatement(removeQuery);
            int rowCount = ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    @FXML 
    void switchScenes(ActionEvent event, String filename) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource(filename));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    
    @FXML
    //Switches scene to Add Tenant Scene
    void switchToAddTenant(ActionEvent event) throws IOException {
        switchScenes(event, "add_tenant.fxml");
        /*FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("add_tenant.fxml"));
        Parent parent = fxmlLoader.load();
        AddTenantController controller = (AddTenantController) fxmlLoader.getController();
        controller.setListView(tenantListView);
        controller.setParent(App.getRoot());
        App.setRoot(parent);*/
    }

    @FXML
    //Switches to Main Menu Scene
    void switchToMainMenu(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("main_menu_final.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

    @FXML
    //Switches to Detailed View scenes
    void switchToViewTenants(ActionEvent event) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("view_tenant.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        TenantViewController controller = fxmlLoader.getController();
        controller.initData(tenantListView.getSelectionModel().getSelectedItem());
        controller.setHostServices(hostServices);
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

}
    