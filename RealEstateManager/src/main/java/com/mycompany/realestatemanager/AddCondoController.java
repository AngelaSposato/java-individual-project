package com.mycompany.realestatemanager;

import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.App;
import com.mycompany.realestatemanager.DBConnection;
import com.mycompany.realestatemanager.DBConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class AddCondoController implements Initializable {
    DBConnection instance = DBConnection.getInstance();
    ObservableList<String> financialInstitutions;
    
    @FXML
    private TextField schoolTax;

    @FXML
    private TextField propertyTax;

    @FXML
    private TextField Insurance;

    @FXML
    private TextField monthlyElectricity;
    
    @FXML
    private TextField condoFees;
    
    @FXML
    private TextField addressText;

    @FXML
    private TextField postalCodeText;

    @FXML
    private TextField numUnitsText;

    @FXML
    private TextField mortgageAmount;

    @FXML
    private ChoiceBox<String> financialInstitutionCB;

    @FXML
    private TextField currMonthlyPayment;

    @FXML
    private DatePicker renewalDate;

    @FXML
    private Button cancelButton;

    @FXML
    private Button confirmButton;
    
    //adds condo to application
    @FXML 
    void addCondo(ActionEvent event) throws IOException, SQLException {
        //Creating objects for property
        Condo newCondo = new Condo(Double.parseDouble(condoFees.getText()), getNewPropertyID(), addressText.getText(), postalCodeText.getText(), "Condo");
        Mortgage newMortgage = new Mortgage(Double.parseDouble(mortgageAmount.getText()), Double.parseDouble(currMonthlyPayment.getText()), financialInstitutionCB.getValue(), renewalDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), newCondo.getPropertyID());
        Taxes newTaxes = new Taxes(Double.parseDouble(schoolTax.getText()),  Double.parseDouble(propertyTax.getText()), Double.parseDouble(monthlyElectricity.getText()), Double.parseDouble(Insurance.getText()), newCondo.getPropertyID());
        
        //Using new objects to insert into DB
        addCondoToDB(newCondo);
        addMortgageToDB(newMortgage, newCondo);
        addTaxesToDB(newCondo, newTaxes);
        
        //GO BACK
        switchToDisplayProperty(event);
    }
    
    //adds condo in property table 
    private void addCondoToDB(Condo newProperty) throws SQLException {
        try {
            String query="insert into property(address, postal_code, property_type) values (?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setString(1, newProperty.getAddress());
            ps.setString(2, newProperty.getPostalCode());
            ps.setString(3, newProperty.getPropertyType());
            ps.execute();
            //instance.getConnection().close();
            System.out.println("Property successfully inserted into database");
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     //Adds mortgage to DB using property_id of condo as foreign key
    private void addMortgageToDB(Mortgage newMortgage, Condo newProperty) throws SQLException {
        try {
            String query="insert into mortgage(property_id, total_amount, monthly_payment, financial_institution, renewal_date) values ((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newMortgage.getTotalAmount());
            ps.setDouble(2, newMortgage.getMonthlyPayment());
            ps.setString(3, newMortgage.getFinancialInstitution());
            ps.setString(4, newMortgage.getRenewalDate());
            ps.execute();
            System.out.println("Mortgage successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //adds taxes to taxes_utilities table in DB using property_id as foreign key
    private void addTaxesToDB(Condo newProperty, Taxes newTaxes) throws SQLException {
        try {
            String query = "insert into taxes_utilities(property_id, school_tax, property_tax, monthly_electricity, insurance, condo_fees) values((select property_id from property where address = \"" + newProperty.getAddress() + "\" ), ?, ?, ?, ?, ?)";
            PreparedStatement ps = instance.getConnection().prepareStatement(query);
            ps.setDouble(1, newTaxes.getSchoolTax());
            ps.setDouble(2, newTaxes.getPropertyTax());
            ps.setDouble(3, newTaxes.getMonthlyElectricity());
            ps.setDouble(4, newTaxes.getInsurance());
            ps.setDouble(5, newProperty.getCondoFees());
            ps.execute();
            System.out.println("Tax info successfully inserted into database");
            ps.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     //Gets highest tenant ID and increments to make a new tenant ID 
    int getNewPropertyID() throws SQLException {
        String query = "select property_id from property where property_id=(select max(property_id) from property)";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        int highestPropertyID = 0;
        while(results.next()){
            highestPropertyID = results.getInt("property_id");
            highestPropertyID++;
        }
        return highestPropertyID;
    }
    
    //Switches to display properties view
    @FXML
    void switchToDisplayProperty(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_properties.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
      financialInstitutions =  FXCollections.observableArrayList();
      financialInstitutions.addAll("CIBC", "RBC", "TD", "BMO");
      financialInstitutionCB.setItems(financialInstitutions);
    }
}
