package com.mycompany.realestatemanager;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * 
 * @author Angela Sposato 1934695
 */
public class PropertyViewController {
    DBConnection instance = DBConnection.getInstance();
    
    private Property selectedProperty;
    
    @FXML
    private Label addressLabel;

    @FXML
    private Label postalCodeLabel;
    
    @FXML
    private Label numUnitsLabel;
     
    @FXML
    private Button viewHistory;

    @FXML
    private Label mortgageLabel;

    @FXML
    private Label fiLabel;

    @FXML
    private Label monthlyPayLabel;

    @FXML
    private Label renewalLabel;

    @FXML
    private Button returnToDisplayPro;

    @FXML
    private Label schoolTax;

    @FXML
    private Label electricityLabel;

    @FXML
    private Label propertyLabel;

    @FXML
    private Label insuranceLabel;

    @FXML
    private Text numUnits;
    
    @FXML
    private Label condoFeesLabel;
    
    @FXML
    private Button modifyMortgageInfo;
    
    @FXML
    private Button modifyTaxInfo;
    
    /*
    @FXML
    void switchToUpdateMortgage(ActionEvent event) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("update_mortgage_info.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        UpdateMortgageController controller = fxmlLoader.getController();
        controller.initData(getMortgageInfo());
       
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    @FXML
    void switchToUpdateTaxes(ActionEvent event) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("update_tax_info.fxml"));
        Parent listViewParent = fxmlLoader.load();
        
        Scene listViewScene = new Scene(listViewParent);
        
        //access controller and call a method
        
        UpdateTaxController controller = fxmlLoader.getController();
        controller.initData(getTaxInfo());
       
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }*/
    
    
    @FXML
    void switchToDisplayProperty(ActionEvent event) throws IOException {
        Parent listViewParent = FXMLLoader.load(getClass().getResource("display_properties.fxml"));
        Scene listViewScene = new Scene(listViewParent);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(listViewScene);
        window.show();
    }
    
    public void initData(Property property) throws SQLException{
        selectedProperty = property;
        addressLabel.setText(selectedProperty.getAddress());
        postalCodeLabel.setText(selectedProperty.getPostalCode());
        numUnitsLabel.setText(getPlexUnits(selectedProperty));
        
        if (selectedProperty.getPropertyType().equals("House") || selectedProperty.getPropertyType().equals("Condo")){
            numUnits.setText("");
        }
        
        if (selectedProperty.getPropertyType().equals("Condo")){
            condoFeesLabel.setText(Double.toString(getCondoFees(selectedProperty)));
        }
        else {
            condoFeesLabel.setText("Not applicable");
        }
        Mortgage newMortgage = getMortgageInfo(selectedProperty);
        mortgageLabel.setText("$" +Double.toString(newMortgage.getTotalAmount()));
        monthlyPayLabel.setText("$" +Double.toString(newMortgage.getMonthlyPayment()));
        fiLabel.setText(newMortgage.getFinancialInstitution());
        renewalLabel.setText(newMortgage.getRenewalDate());
        
        Taxes newTaxes = getTaxInfo(selectedProperty);
        schoolTax.setText("$" +Double.toString(newTaxes.getSchoolTax()));
        propertyLabel.setText("$" +Double.toString(newTaxes.getPropertyTax()));
        electricityLabel.setText("$" +Double.toString(newTaxes.getMonthlyElectricity()));
        insuranceLabel.setText("$" +Double.toString(newTaxes.getInsurance()));
    }
    
    public Mortgage getMortgageInfo(Property property) throws SQLException {
        Mortgage aMortgage = null;
        String query = "select total_amount, monthly_payment, financial_institution, renewal_date, property_id from mortgage where property_id = \"" + property.getPropertyID() + "\"";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        while(results.next()) {
            double totalAmount = results.getDouble("total_amount");
            double monthlyPayment = results.getDouble("monthly_payment");
            String financialInstitution = results.getString("financial_institution");
            String renewalDate = results.getString("renewal_date");
            int mortgageID = results.getInt("property_id");
            aMortgage = new Mortgage(totalAmount, monthlyPayment, financialInstitution, renewalDate, mortgageID);
            
        }
        return aMortgage;
    }
        
    public Taxes getTaxInfo(Property property) throws SQLException {
        Taxes aTaxes = null;
        String query = "select school_tax, property_tax, monthly_electricity, insurance, condo_fees, property_id from taxes_utilities where property_id = \"" + property.getPropertyID() + "\"";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        while(results.next()) {
            double schoolTax = results.getDouble("school_tax");
            double propertyTax = results.getDouble("property_tax");
            double monthlyElectricity = results.getDouble("monthly_electricity");
            double insurance = results.getDouble("insurance");
            double condoFees = results.getDouble("condo_fees");
            int taxID = results.getInt("property_id");
            aTaxes = new Taxes(schoolTax, propertyTax, monthlyElectricity, insurance, taxID);
        }
        return aTaxes;
    }

    private String getPlexUnits(Property property) throws SQLException {
        String numUnits = "";
        String query = "select number_units from property where property_id =\"" + property.getPropertyID() + "\"";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        while(results.next()){
            numUnits = results.getString("number_units");
        }
        return numUnits;
    }
    
    private double getCondoFees(Property property) throws SQLException {
        double condoFees = 0;
        String query = "select condo_fees from taxes_utilities where property_id=\"" + property.getPropertyID() + "\"";
        PreparedStatement ps = instance.getConnection().prepareStatement(query);
        ResultSet results = ps.executeQuery();
        while(results.next()){
            condoFees = results.getDouble("condo_fees");
        }
        return condoFees;
    }
    
    @FXML
    void switchToMaintenanceHistory(ActionEvent event) {
        //TODO
    }

}
