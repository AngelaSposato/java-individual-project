/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Angela Sposato 1934695
 */
public class FinancialInstitutionTest {
    
    public FinancialInstitutionTest() {
    }

    /**
     * Test of getName method, of class FinancialInstitution.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        FinancialInstitution instance = new FinancialInstitution(1, "RBC", "514-555-5555", 1, "rbc@rbc.com", "rbcbank.ca");
        String expResult = "RBC";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhone method, of class FinancialInstitution.
     */
    @Test
    public void testGetPhone() {
        System.out.println("getPhone");
        FinancialInstitution instance = new FinancialInstitution(1, "RBC", "514-555-5555", 1, "rbc@rbc.com", "rbcbank.ca");
        String expResult = "514-555-5555";
        String result = instance.getPhone();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInterestRate method, of class FinancialInstitution.
     */
    @Test
    public void testGetInterestRate() {
        System.out.println("getInterestRate");
        FinancialInstitution instance = new FinancialInstitution(1, "RBC", "514-555-5555", 1, "rbc@rbc.com", "rbcbank.ca");
        double expResult = 1;
        double result = instance.getInterestRate();
        assertEquals(expResult, result);
    }
}
