/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Angela Sposato 1934695
 */
public class MortgageTest {
    
    /**
     * Test of getTotalAmount method, of class Mortgage.
     */
    @Test
    public void testGetTotalAmount() {
        System.out.println("getTotalAmount");
        Mortgage instance = new Mortgage(500000, 1000, "CIBC", "2021-05-25", 1);
        double expResult = 500000;
        double result = instance.getTotalAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonthlyPayment method, of class Mortgage.
     */
    @Test
    public void testGetMonthlyPayment() {
        System.out.println("getMonthlyPayment");
        Mortgage instance = new Mortgage(500000, 1000, "CIBC", "2021-05-25", 1);
        double expResult = 1000;
        double result = instance.getMonthlyPayment();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFinancialInstitution method, of class Mortgage.
     */
    @Test
    public void testGetFinancialInstitution() {
        System.out.println("getFinancialInstitution");
        Mortgage instance = new Mortgage(500000, 1000, "CIBC", "2021-05-25", 1);
        String expResult = "CIBC";
        String result = instance.getFinancialInstitution();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenewalDate method, of class Mortgage.
     */
    @Test
    public void testGetRenewalDate() {
        System.out.println("getRenewalDate");
        Mortgage instance = new Mortgage(500000, 1000, "CIBC", "2021-05-25", 1);
        String expResult = "2021-05-25";
        String result = instance.getRenewalDate();
        assertEquals(expResult, result);
    }
}
