/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Angela Sposato 1934695
 */
public class PropertyTest {
    

    /**
     * Test of getAddress method, of class Property.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Property instance = new Property(1, "4600 POW", "H4B 2H4", "House");
        String expResult = "4600 POW";
        String result = instance.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPostalCode method, of class Property.
     */
    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        Property instance = new Property(1, "4600 POW", "H4B 2H4", "House");
        String expResult = "H4B 2H4";
        String result = instance.getPostalCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyType method, of class Property.
     */
    @Test
    public void testGetPropertyType() {
        System.out.println("getPropertyType");
        Property instance = new Property(1, "4600 POW", "H4B 2H4", "House");
        String expResult = "House";
        String result = instance.getPropertyType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberUnits method, of class Property.
     */
}
