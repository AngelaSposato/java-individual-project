/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Angela Sposato 1934695
 */
public class ContractorTest {
    
    public ContractorTest() {
    }

    /**
     * Test of getFirstName method, of class Contractor.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "Busy guy");
        String expResult = "Angela";
        String result = instance.getFirstName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLastName method, of class Contractor.
     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "busy guy");
        String expResult = "Sposato";
        String result = instance.getLastName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhone method, of class Contractor.
     */
    @Test
    public void testGetPhone() {
        System.out.println("getPhone");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "busy guy");
        String expResult = "514-555-5555";
        String result = instance.getPhone();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Contractor.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "busy guy");
        String expResult = "angela@dawson.qc.ca";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTrade method, of class Contractor.
     */
    @Test
    public void testGetTrade() {
        System.out.println("getTrade");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "busy guy");
        String expResult = "Plumber";
        String result = instance.getTrade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEnterprise method, of class Contractor.
     */
    @Test
    public void testGetEnterprise() {
        System.out.println("getEnterprise");
        Contractor instance = new Contractor(1, "Angela", "Sposato", "514-555-5555", "angela@dawson.qc.ca", "Plumber", "ATO Construction", "busy guy");
        String expResult = "ATO Construction";
        String result = instance.getEnterprise();
        assertEquals(expResult, result);
    }
}
