/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Angela Sposato 1934695
 */
public class TenantTest {
    
    /**
     * Test of getFirstName method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments here");
        String expResult = "Angela";
        String result = instance.getFirstName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLastName method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetLastName() {
        System.out.println("getLastName");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments here");
        String expResult = "Sposato";
        String result = instance.getLastName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments here");
        String expResult = "angela@dawsoncollege.qc.ca";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhone method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetPhone() {
        System.out.println("getPhone");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments here");
        String expResult = "514-555-5555";
        String result = instance.getPhone();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAddressRented method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetAddressRented() {
        System.out.println("getAddressRented");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments");
        String expResult = "4600 POW";
        String result = instance.getAddressRented();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUnitRented method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetUnitRented() {
        System.out.println("getUnitRented");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments");
        String expResult = "1";
        String result = instance.getUnitRented();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonthlyRent method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetMonthlyRent() {
        System.out.println("getMonthlyRent");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments");
        double expResult = 500;
        double result = instance.getMonthlyRent();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenewalDate method, of class Tenant.
     */
    @org.junit.jupiter.api.Test
    public void testGetRenewalDate() {
        System.out.println("getRenewalDate");
        Tenant instance = new Tenant(1, "Angela", "Sposato", "angela@dawsoncollege.qc.ca", "514-555-5555", "4600 POW", "1", 500, "2021-04-25", "Additional comments");
        String expResult = "2021-04-25";
        String result = instance.getRenewalDate();
        assertEquals(expResult, result);
    }    
}
