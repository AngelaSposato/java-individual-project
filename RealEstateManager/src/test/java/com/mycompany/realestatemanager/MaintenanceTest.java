/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestatemanager;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Angela Sposato 1934695
 */
public class MaintenanceTest {
    
    public MaintenanceTest() {
    }

    /**
     * Test of getMaintenanceName method, of class Maintenance.
     */
    @Test
    public void testGetMaintenanceName() {
        System.out.println("getMaintenanceName");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "New Maintenance";
        String result = instance.getMaintenanceName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAddress method, of class Maintenance.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "4600 POW";
        String result = instance.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUnit method, of class Maintenance.
     */
    @Test
    public void testGetUnit() {
        System.out.println("getUnit");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "1";
        String result = instance.getUnit();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorName method, of class Maintenance.
     */
    @Test
    public void testGetContractorName() {
        System.out.println("getContractorName");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "Angela Sposato";
        String result = instance.getContractorName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorType method, of class Maintenance.
     */
    @Test
    public void testGetContractorType() {
        System.out.println("getContractorType");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "Plumber";
        String result = instance.getContractorType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCost method, of class Maintenance.
     */
    @Test
    public void testGetCost() {
        System.out.println("getCost");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        int expResult = 400;
        int result = instance.getCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class Maintenance.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        String expResult = "2021-04-25";
        String result = instance.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of isMarkAsComplete method, of class Maintenance.
     */
    @Test
    public void testIsMarkAsComplete() {
        System.out.println("isMarkAsComplete");
        Maintenance instance = new Maintenance(1, "New Maintenance", "4600 POW", "1", "Angela Sposato", "Plumber", 400, "2021-04-25", false);
        boolean expResult = false;
        boolean result = instance.isMarkAsComplete();
        assertEquals(expResult, result);
    }    
}
